<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'guest',
                'created_at' => now()
            ],
            [
                'name' => 'user',
                'created_at' => now()
            ],
            [
                'name' => 'admin',
                'created_at' => now()
            ]
        ]);
    }
}
