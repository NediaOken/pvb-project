<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            [
                'content' => 'Dit is een test blog.',
                'user_id' => '1',
                'blog_id' => '1',
                'created_at' => now()
            ],
            [
                'content' => 'Dit is een 2de test blog.',
                'user_id' => '2',
                'blog_id' => '2',
                'created_at' => now()
            ]
        ]);
    }
}
